from flask import Flask, render_template, url_for, request, redirect
from flask.json import tojson_filter
import time, datetime
app = Flask(__name__)

doc_1_dic = {}
doc_2_dic = {}
doc_3_dic = {}


@app.route('/')
def index():
    return render_template('pacientes.html')

@app.route('/hay_pacientes', methods=['GET', 'POST'])
def hay_pacientes():
    global doc_1_dic
    global doc_2_dic
    global doc_3_dic
    paci_1 = doc_1_dic
    paci_2 = doc_2_dic
    paci_3 = doc_3_dic
    while not paci_1 or not paci_2 or not paci_3:
        pacis = "No hay pacientes"
        time.sleep(30)
        return pacis

@app.route('/panel_medicos', methods=['GET', 'POST'])
def panel_medicos():
    global doc_1_dic
    global doc_2_dic
    global doc_3_dic
    panel_medico = request.form.get("panel_medico")
    print(panel_medico)
    if panel_medico == "pm1":
        return redirect(url_for('medico_1'))
    elif panel_medico == "pm2":
        return redirect(url_for('medico_2'))
    elif panel_medico == "pm3":
        return redirect(url_for('medico_3'))
    else:
        return render_template('medicos.html')

@app.route('/medico_1')
def medico_1():
    global doc_1_dic
    global doc_2_dic
    global doc_3_dic
    paci = doc_1_dic
    return render_template("medico_1.html",paci=paci)

@app.route('/medico_1_atendio')
def medico_1_atendio():
    global doc_1_dic
    global doc_2_dic
    global doc_3_dic
    doc_1_dic.clear
    deso = "Se ha atentido al paciente"
    return render_template("medico_1_2.html",deso=deso)

@app.route('/medico_1_descanso')
def medico_1_descanso():
    global doc_1_dic
    global doc_2_dic
    global doc_3_dic
    paci = doc_1_dic
    global timer_running, timerint, startime
    timer_running = True
    inicio = time.time()
    momento = time.time()
    if paci == None:
        paci = "No tiene pacientes dispoibles"
    while (momento - inicio > 20 * 60):
        doc_1_dic = {'no disponible'}
        time.sleep(1200)
        if momento - inicio > 20 * 60:
            doc_1_dic.clear()
            break
    return render_template("medico_1_3.html",paci=paci)

@app.route('/medico_2')
def medico_2():
    global doc_1_dic
    global doc_2_dic
    global doc_3_dic
    paci = doc_2_dic
    return render_template("medico_2.html",paci=paci)

@app.route('/medico_2_atendio')
def medico_2_atendio():
    global doc_1_dic
    global doc_2_dic
    global doc_3_dic
    doc_2_dic.clear
    deso = "Se ha atentido al paciente"
    return render_template("medico_2_2.html",deso=deso)

@app.route('/medico_2_descanso')
def medico_2_descanso():
    global doc_1_dic
    global doc_2_dic
    global doc_3_dic
    paci = doc_2_dic
    global timer_running, timerint, startime
    timer_running = True
    inicio = time.time()
    momento = time.time()
    if paci == None:
        paci = "No tiene pacientes dispoibles"
    while (momento - inicio > 20 * 60):
        doc_1_dic = {'no disponible'}
        time.sleep(1200)
        if momento - inicio > 20 * 60:
            doc_2_dic.clear()
            break
    return render_template("medico_2_3.html",paci=paci)
@app.route('/medico_3')
def medico_3():
    global doc_1_dic
    global doc_2_dic
    global doc_3_dic
    paci = doc_3_dic
    return render_template("medico_3.html",paci=paci)

@app.route('/medico_3_atendio')
def medico_3_atendio():
    global doc_1_dic
    global doc_2_dic
    global doc_3_dic
    doc_3_dic.clear
    deso = "Se ha atentido al paciente"
    return render_template("medico_3_2.html",deso=deso)

@app.route('/medico_3_descanso')
def medico_3_descanso():
    global doc_1_dic
    global doc_2_dic
    global doc_3_dic
    paci = doc_3_dic
    global timer_running, timerint, startime
    timer_running = True
    inicio = time.time()
    momento = time.time()
    if paci == None:
        paci = "No tiene pacientes dispoibles"
    while (momento - inicio > 20 * 60):
        doc_3_dic = {'no disponible'}
        time.sleep(1200)
        if momento - inicio > 20 * 60:
            doc_3_dic.clear()
            break
    return render_template("medico_3_3.html",paci=paci)

@app.route('/guardar_turno', methods=['GET', 'POST'])
def form_carga_guardar():
    hay_pacientes()
    global doc_1_dic
    global doc_2_dic
    global doc_3_dic
    # obtengo los datos cargados desde la pagina form_carga
    nombre = request.form.get("nombre")
    edad = request.form.get("edad")
    sexo = request.form.get("sexo")
    hijos = request.form.get("hijos")
    dolencias = request.form.get("dolencias")
    medico = request.form.get("medico")
    datos_pac = []
    datos_pac = nombre, edad, sexo, hijos
    if not bool(medico):
        print("fallo medico")
    else:
        if medico == "m1":
            doc_1_dic = {'nombre': nombre, 'edad': edad,
                        'sexo': sexo, 'hijos': hijos,'dolencias':dolencias}
            print("el doc_1 fue elegido")
            print(doc_1_dic)
        elif medico == "m2":
            doc_2_dic = {'nombre': nombre, 'edad': edad,
                        'sexo': sexo, 'hijos': hijos, 'dolencias':dolencias}
            print("el doc_2 fue elegido")
            print(doc_2_dic)
        else:
            doc_3_dic = {'nombre': nombre, 'edad': edad,
                        'sexo': sexo, 'hijos': hijos, 'dolencias':dolencias}
            print("el doc_3 fue elegido")
            print(doc_3_dic)

    if doc_1_dic and doc_2_dic and doc_3_dic:
        print(bool(doc_1_dic),bool(doc_2_dic),bool(doc_3_dic))
        return render_template('pacientes_7.html')
    elif doc_2_dic and doc_3_dic and not doc_1_dic:
        print(bool(doc_2_dic),bool(doc_3_dic),"el doc_2 y doc_3 entro")
        return render_template('pacientes_6.html')
    elif doc_1_dic and doc_3_dic and not doc_2_dic:
        print(bool(doc_1_dic),bool(doc_3_dic),"el doc_1 y doc_3 entro")
        return render_template('pacientes_5.html')
    elif doc_1_dic and doc_2_dic and not doc_3_dic:
        print(bool(doc_1_dic),bool(doc_2_dic),"el doc_1 y doc_2 entro")
        return render_template('pacientes_4.html')
    elif doc_3_dic and not doc_1_dic and not doc_2_dic:
        print(bool(doc_3_dic),"el doc_3 entro")
        return render_template('pacientes_3.html')
    elif doc_2_dic and not doc_1_dic and not doc_3_dic:
        print(bool(doc_2_dic),"el doc_2 entro")
        return render_template('pacientes_2.html')
    elif doc_1_dic and not doc_2_dic and not doc_3_dic:
        print(bool(doc_1_dic),"el doc_1 entro")
        return render_template('pacientes_1.html')
    else:
        return render_template('pacientes.html')

if __name__ == '__main__':
    app.run(host='127.0.0.1', port='80', debug=True)
